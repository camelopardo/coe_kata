﻿using System.Collections.Generic;

namespace CoE_Challenge.Models
{
    public class Order
    {
        public List<OrderLine> Lines { get; private set; }

        public Order()
        {
            Lines = new List<OrderLine>();
        }
    }
}