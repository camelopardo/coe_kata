﻿namespace CoE_Challenge.Models
{
    public class Item
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public ProductCategories Category { get; set; }
    }

    public enum ProductCategories
    {
        Toilettries = 1,
        Snacks = 2,
        Beverages = 3,
        Sauces = 4
    }

}