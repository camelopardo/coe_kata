﻿using System.Collections.Generic;

namespace CoE_Challenge.Models
{
    public class OrderLine
    {
        public Item Item { get; set; }
        public int Quantity { get; set; }
    }
}