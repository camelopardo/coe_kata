﻿using CoE_Challenge.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoE_Challenge
{
    public class Discounts
    {
        public decimal Get2x1Discount(OrderLine line)
        {
            if (line.Item.Category == ProductCategories.Snacks)
            {
                return line.Quantity * line.Item.Price;
            }
            return line.Item.Price * (line.Quantity / 2) + line.Item.Price * (line.Quantity % 2); // Got from: https://tinyurl.com/3hxnmdbt
        }

        public OrderLine AddFreeSecondItem(OrderLine item1, OrderLine item2)
        {
            return new OrderLine()
            {
                Item = new Item() { Name = item2.Item.Name, Category = item2.Item.Category, Price = 0 },
                Quantity = (item2.Quantity >= item1.Quantity) ? item1.Quantity : item2.Quantity
            };
        }
    }
}
