﻿using CoE_Challenge.Models;
using System.Linq;

namespace CoE_Challenge.Data
{
    public class OrderData
    {
        public Order GetOrderData()
        {
            var shampoo = new Item { Name = "Shampoo", Price = 12.95m, Category = ProductCategories.Toilettries };
            var soap = new Item { Name = "Soap", Price = 8m, Category = ProductCategories.Toilettries };
            var nachos = new Item { Name = "Nachos", Price = 7m, Category = ProductCategories.Snacks };
            var soda = new Item { Name = "Soda (2 lts)", Price = 13.50m, Category = ProductCategories.Beverages };
            var chips = new Item { Name = "Potato chips", Price = 10m, Category = ProductCategories.Snacks };
            var dip = new Item { Name = "Dip", Price = 10m, Category = ProductCategories.Sauces };

            var order = new Order();
            order.Lines.Add(new OrderLine { Item = shampoo, Quantity = 4 });
            order.Lines.Add(new OrderLine { Item = soap, Quantity = 5 });
            order.Lines.Add(new OrderLine { Item = nachos, Quantity = 2 });
            order.Lines.Add(new OrderLine { Item = soda, Quantity = 1 });
            order.Lines.Add(new OrderLine { Item = chips, Quantity = 1 });

            order = GetExtras(order, shampoo.Name, soap.Name);
            order = GetExtras(order, soda.Name, chips.Name);
            order = AddDipLine(order);

            return order;
        }

        private static Order AddDipLine(Order order)
        {
            var freeDip = new Item { Name = "Dip", Price = 0m, Category = ProductCategories.Sauces };
            OrderLine nachoOrder = order.Lines.FirstOrDefault(x => string.Equals(x.Item.Name, "Nachos"));
            if (nachoOrder.Quantity >= 2) { order.Lines.Add(new OrderLine() { Item = freeDip, Quantity = 1 }); }
            return order;
        }

        private static Order GetExtras(Order order, string item1, string item2)
        {
            OrderLine order1 = order.Lines.FirstOrDefault(x => string.Equals(x.Item.Name, item1));
            OrderLine order2 = order.Lines.FirstOrDefault(x => string.Equals(x.Item.Name, item2));
            var discounts = new Discounts();
            order.Lines.Add(discounts.AddFreeSecondItem(order1, order2));
            return order;
        }
    }
}